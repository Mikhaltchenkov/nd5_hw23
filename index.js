const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));

app.get('/', (req, res) => {
    res.send('Hello, Express.js');
});

app.get('/hello', (req, res) => {
    res.send('Hello stranger!');
});

app.get('/hello/:name', (req, res) => {
    res.send(`Hello, ${req.params.name}!`);
});

app.all('/sub/*', (req, res) => {
    res.send(`You requested URI: ${req.originalUrl}`);
});

const checkHeader = function (req, res, next) {
    if (req.headers.header == 'Key') {
        next();
    } else {
        res.status(401).send('Not authorized!');
    }
};

app.post('/post', checkHeader, (req, res) => {
    if (Object.keys(req.body).length === 0) {
        res.status(404).send('Not Found');
    } else {
        res.json(req.body);
    }
});

app.listen(3000,() => {
   console.log('Server started on port 3000');
});